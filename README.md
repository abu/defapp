# Default App

**[ownCloud](https://owncloud.org) app for selecting the default app via web frontend**

This app introduces an option to set the default app from the web frontend. It's located at the _Settings->Admin_ section under _Additional_.

I've no clue, why this is not implemented in ownCloud server out of the box.

![screenshot](https://codeberg.org/abu/defapp/raw/branch/master/screenshots/defapp.png)

## Known limitation
Any menu items created by the app _External Sites_ cannot be selected, they will be omitted in the select dropdown.
