module.exports = {
  'env': {
    'browser': true,
    'es2021': true,
  },
  'extends': [
    'eslint:recommended',
    'plugin:es-beautifier/standard',
  ],
  'parserOptions': {
    'ecmaVersion': 12,
    'sourceType': 'module',
  },
  'plugins': [
    'es-beautifier',
  ],
  'rules': {
    'quotes': ['error', 'single'],
    'no-console': 'warn',
  },
  'globals': {
    'module': 'readonly',
    'OC': 'readonly',
    '$': 'readonly',
    't': 'readonly',
    'oc_requesttoken': 'readonly',
  },
};
