<?php
namespace OCA\DefApp\AppInfo;

use OCA\DefApp\DefAppConfig;
use OCP\AppFramework\App;

class Application extends App {
	public function __construct(array $urlParams=[]) {
		parent::__construct('defapp', $urlParams);
	}
}
