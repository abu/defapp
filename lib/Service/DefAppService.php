<?php

namespace OCA\DefApp\Service;

use OCP\IConfig;
use OCP\INavigationManager;

class DefAppService {
	private const DEFAULTAPP = 'defaultapp';
	private $config;
	private $navmgr;

	/**
	 * @param IConfig $config
	 */
	public function __construct(IConfig $config, INavigationManager $navmgr) {
		$this->config = $config;
		$this->navmgr =$navmgr;
	}

	/**
	 * @return string $appId
	 */
	public function getDefaultAppId() {
		return $this->config->getSystemValue(self::DEFAULTAPP);
	}

	/**
	 * @param string $appId
	 */
	public function setDefaultAppId($appId) {
		$this->config->setSystemValue(self::DEFAULTAPP, $appId);
	}

	/**
	 * @return Array
	 */
	public function getAppList() {
		$navlist = $this->navmgr->getAll();
		$applist = [];
		foreach ($navlist as $app) {
			// Ignore items of app external sites
			if (\strpos($app['id'], 'external_index', 0) !== false) {
				continue;
			}
			\array_push($applist, $app['id']);
		}
		\sort($applist);
		return $applist;
	}
}
