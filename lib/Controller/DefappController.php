<?php

namespace OCA\DefApp\Controller;

use OCA\DefApp\Service\DefAppService;
use OCP\AppFramework\Controller;
use OCP\AppFramework\Http\DataResponse;
use OCP\IRequest;

class DefappController extends Controller {
	private $service;

	public function __construct($appName, IRequest $request, DefAppService $service) {
		parent::__construct($appName, $request);
		$this->service = $service;
	}

	/**
	 * @return DataResponse
	 */
	public function get() {
		return new DataResponse(
			$this->service->getDefaultAppId()
		);
	}

	/**
	 * @param string $appId
	 */
	public function set($appId) {
		$this->service->setDefaultAppId($appId);
	}
}
