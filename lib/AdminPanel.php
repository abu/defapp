<?php

namespace OCA\DefApp;

use OCA\DefApp\Service\DefAppService;
use OCP\Settings\ISettings;
use OCP\Template;

class AdminPanel implements ISettings {
	public const PRIORITY = 10;

	private $service;

	public function __construct(DefAppService $service) {
		$this->service = $service;
	}

	public function getSectionID() {
		return 'additional';
	}

	public function getPriority() {
		return self::PRIORITY;
	}

	public function getPanel() {
		$tmpl =  new Template('defapp', 'AdminPanel');
		$tmpl->assign('defaultapp', $this->service->getDefaultAppId());
		$tmpl->assign('apps', $this->service->getAppList());
		return $tmpl;
	}
}
