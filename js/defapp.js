'use strict';

async function setDefaultApp(appId) {
  var url = OC.generateUrl('/apps/defapp') + '?appId=' + appId;

  let response = await fetch(url, {
    method: 'POST',
    headers: {
      'requesttoken': oc_requesttoken,
    },
  });
  if (response.ok) {
    await response.text();
  } else {
    OC.Notification.showTemporary(t('defapp', 'Request failed'));
  }
}

$(document).ready(function () {
  $('#as-save').click(function (e) {
    e.preventDefault();

    try {
      let appId = $('#appid').children('option:selected').text();
      setDefaultApp(appId);
      OC.msg.finishedSuccess('#defapp-message', t('defapp', 'Default app updated'));
    } catch (msg) {
      OC.msg.finishedError('#defapp-message', t('defapp', msg));
    }
  });
});
