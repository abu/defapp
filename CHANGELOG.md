# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

- Nothing so far

## [v0.1.1](https://codeberg.org/abu/defapp/releases/tag/v0.1.1) - 2023-09-30

### Fixed

- Sending request tokens is now mandatory.

## [v0.1.0](https://codeberg.org/abu/defapp/releases/tag/v0.1.0) - 2022-07-04

- Initial release

---
