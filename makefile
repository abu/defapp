
.PHONY: all appstore build clean lint  install update

APP_NAME = defapp

CERT_DIR=$(HOME)/.owncloud
PRIVATE_KEY=$(CERT_DIR)/$(APP_NAME).key
CERTIFICATE=$(CERT_DIR)/$(APP_NAME).crt

CONFIG_DIR=../../config
OCC=../../occ

BUILD_DIR=$(CURDIR)/build

APPSTORE_BUILD_DIR=$(BUILD_DIR)/$(APP_NAME)
PACKAGE_FILE=$(APPSTORE_BUILD_DIR)

SIGN=php -f $(OCC) integrity:sign-app --privateKey="$(PRIVATE_KEY)" --certificate="$(CERTIFICATE)"
SIGN_SKIP_MSG="*** Signing skipped due to missing files."

ifneq (,$(wildcard $(PRIVATE_KEY)))
ifneq (,$(wildcard $(CERTIFICATE)))
ifneq (,$(wildcard $(OCC)))
CAN_SIGN=true
endif
endif
endif

lint:
	npm run lint

all: test-php-style lint appstore

install:
	composer install
	npm install

clean:
	rm -rf $(BUILD_DIR)

###############################################################################

build-dir:
	mkdir -p $(APPSTORE_BUILD_DIR)

appstore: clean build-dir $(BUILD_DIR)/appstore-exclude
	rsync -r . $(APPSTORE_BUILD_DIR) --exclude-from=$(BUILD_DIR)/appstore-exclude
ifdef CAN_SIGN
	mv $(CONFIG_DIR)/config.php $(CONFIG_DIR)/config-2.php
	$(SIGN) --path="$(APPSTORE_BUILD_DIR)"
	mv $(CONFIG_DIR)/config-2.php $(CONFIG_DIR)/config.php
else
	@echo $(SIGN_SKIP_MSG)
endif
	tar -czf $(PACKAGE_FILE).tar.gz -C $(BUILD_DIR) $(APP_NAME)

###############################################################################
# Create exclude list for rsync

.ONESHELL:
$(BUILD_DIR)/appstore-exclude:
	@cat <<- EOF > $@
		.eslintrc.js
		.git
		.gitignore
		.php-cs-fixer.dist.php
		.php-cs-fixer.cache
		makefile
		build/
		composer.json
		composer.lock
		install
		node_modules/
		package-lock.json
		package.json
		screenshots/
		src/
		tools/
		vendor/
	EOF

###############################################################################
# Run php-cs-fixer and check owncloud code-style

PHP_CS_FIXER=php -d zend.enable_gc=0 vendor/bin/php-cs-fixer fix -v --diff \
	--allow-risky yes

.PHONY: test-php-style
test-php-style:	vendor/bin/php-cs-fixer
	$(PHP_CS_FIXER) --dry-run

.PHONY: test-php-style-fix
test-php-style-fix:	vendor/bin/php-cs-fixer
	$(PHP_CS_FIXER)

###############################################################################
