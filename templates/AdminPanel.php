<?php
style('defapp', 'defapp');
script('defapp', 'defapp');
?>

<form class='section'>
	<h2 chass='app-name'>Default App</h2>
	<div>
		<label class='input' for='appid'>AppId</label>
		<select class='select setting panel' id='appid'  >
 			<?php foreach ($_['apps'] as $appId) { ?>
				<option value='<?php p($appId) ?>'<?php p($appId === $_['defaultapp'] ? 'selected' : '') ?>>
					<?php p($appId) ?>
				</option>
		 	<?php } ?>
		</select>
	</div>
	<button id="as-save">Save</button>
	<span id="defapp-message" class="msg"></span>
</form>
